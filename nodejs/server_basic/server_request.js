var http = require('http');
var url = require('url');

var server = http.createServer(function(request,response){
    console.log(request.url);

    var parsedUrl = url.parse(request.url);

    var resource = parsedUrl.pathname;

    console.log('resource path = %s', resource);

    if(resource == '/address'){
        response.writeHead(200, {'Content-Type':'text/html'});
        response.end('Incheon')
    }
    else if(resource == '/abc'){
        response.writeHead(200, {'Content-Type':'text/html'});
        response.end('abc');
    }
    else if(resource == '/RF/BR1/RU0'){
        response.writeHead(200, {'Content-Type' : 'text/html'});
        response.end('ok')
    }
    else{
        response.writeHead(404, {'Content-Type':'text/html'});
        response.end('404 Page Not Found');
    }
});

server.listen(80,function(){
    console.log('Server is running...');
})